package com.dowjones.codechallenge.helper

import com.google.gson.Gson
import com.google.gson.GsonBuilder

object JsonParser {
    private var parser: Gson? = null

    @get:Synchronized
    val instance: Gson
        get() {
            if (parser == null) {
                parser = GsonBuilder().create()
            }
            return parser!!
        }

}
