package com.dowjones.codechallenge.helper

open class Configure {
    companion object {
        val BASE_API_DOMAIN: String = "http://jsonplaceholder.typicode.com/"
        val CONNECTION_TIMEOUT_IN_SECOND: Long = 60
    }
}