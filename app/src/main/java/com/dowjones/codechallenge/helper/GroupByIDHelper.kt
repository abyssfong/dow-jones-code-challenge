package com.dowjones.codechallenge.helper

import com.dowjones.codechallenge.dto.CodeChallengeDto
import com.dowjones.codechallenge.model.GroupByIdModel


fun List<CodeChallengeDto>.groupById() : List<GroupByIdModel>{
    return this.groupBy{it.albumId}.entries.map { (name,group)-> GroupByIdModel(name,group) }
}

