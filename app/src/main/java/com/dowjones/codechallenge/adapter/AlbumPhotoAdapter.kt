package com.dowjones.codechallenge.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.dowjones.codechallenge.databinding.CellInsideAdapterItemBinding
import com.dowjones.codechallenge.dto.CodeChallengeDto
import com.dowjones.codechallenge.util.HttpHeaderUtil.Companion.addHeaderUrl

class AlbumPhotoAdapter : RecyclerView.Adapter<AlbumPhotoAdapter.AlbumPhotoAdapterHorder>() {
    private var dataList: List<CodeChallengeDto>? = null
    var onItemClick: ((String) -> Unit)? = null
    var context:Context? =null


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): AlbumPhotoAdapter.AlbumPhotoAdapterHorder {
        val binding = CellInsideAdapterItemBinding.inflate(
            LayoutInflater.from(parent.context),
            parent,false
        )
        context = parent.context
        return AlbumPhotoAdapterHorder(binding)
    }

    override fun onBindViewHolder(holder:AlbumPhotoAdapterHorder, position: Int) {
        if (dataList?.size ?: 0 - 1 > position) {
            dataList?.get(position)?.let { holder.init(it,position) }
        }
    }

    override fun getItemCount(): Int {
        return dataList?.size ?: 0
    }

    fun updateData(data: List<CodeChallengeDto>) {
        dataList = data
        notifyDataSetChanged()
    }

    inner class AlbumPhotoAdapterHorder(val binding: CellInsideAdapterItemBinding) :
        RecyclerView.ViewHolder(binding.root) {
        fun init(data: CodeChallengeDto, position: Int) {
            binding.widgetAlbumPhotoInsideView
                .setAction { onItemClick?.invoke(data.url) }
            binding.widgetAlbumPhotoInsideView.setImage(addHeaderUrl(data.thumbnailUrl),context!!)
            binding.widgetAlbumPhotoInsideView.setTitle(data.title)
            }
    }
}