package com.dowjones.codechallenge.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.dowjones.codechallenge.R
import com.dowjones.codechallenge.databinding.CellMainAdapterItemBinding
import com.dowjones.codechallenge.model.GroupByIdModel
import com.dowjones.codechallenge.util.HttpHeaderUtil.Companion.addHeaderUrl

class MainPagePhotoAdapter : RecyclerView.Adapter<MainPagePhotoAdapter.MainPagePhotoListHorder>() {
    private var dataList: List<GroupByIdModel>? = null
    var onItemClick: ((Int) -> Unit)? = null
    var context:Context? =null


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MainPagePhotoAdapter.MainPagePhotoListHorder {
        val binding = CellMainAdapterItemBinding.inflate(
            LayoutInflater.from(parent.context),
            parent,false
        )
        context = parent.context
        return MainPagePhotoListHorder(binding)
    }

    override fun onBindViewHolder(holder:MainPagePhotoListHorder, position: Int) {
        if (dataList?.size ?: 0 - 1 > position) {
            dataList?.get(position)?.let { holder.init(it,position) }
        }
    }

    override fun getItemCount(): Int {
        return dataList?.size ?: 0
    }

    fun updateData(data: List<GroupByIdModel>) {
        dataList = data
        notifyDataSetChanged()
    }

    inner class MainPagePhotoListHorder(val binding: CellMainAdapterItemBinding) :
        RecyclerView.ViewHolder(binding.root) {
        fun init(data: GroupByIdModel, position: Int) {
            binding.widgetAlbumDisplayView.setAction { onItemClick?.invoke(position) }
            binding.widgetAlbumDisplayView.setImage(addHeaderUrl(data.imgData[0].thumbnailUrl),context!!)
            binding.widgetAlbumDisplayView.setCount(dataList?.size.toString())
            binding.widgetAlbumDisplayView.setTitle(context!!.getString(R.string.albumId,(position+1).toString()))
            }
    }
}