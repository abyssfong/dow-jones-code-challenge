package com.dowjones.codechallenge.connection

import com.dowjones.codechallenge.helper.Configure
import com.dowjones.codechallenge.helper.JsonParser
import okhttp3.Interceptor
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit



class RetrofitClient {

    private var mRetrofit: Retrofit? = null

    @Synchronized
    fun getDefaultInstance(): Retrofit? {
        if (mRetrofit == null) {
            mRetrofit = Retrofit.Builder()
                .baseUrl(Configure.BASE_API_DOMAIN)
                .client(getDefaultOkHttpClientBuilder())
                .addConverterFactory(
                    GsonConverterFactory.create(
                        JsonParser.instance
                    )
                )
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .build()
        }
        return mRetrofit
    }

     fun getDefaultOkHttpClientBuilder(): OkHttpClient {
        val builder = OkHttpClient.Builder()
            .retryOnConnectionFailure(true)
            .readTimeout(Configure.CONNECTION_TIMEOUT_IN_SECOND, TimeUnit.SECONDS)
            .writeTimeout(Configure.CONNECTION_TIMEOUT_IN_SECOND, TimeUnit.SECONDS)
            .connectTimeout(Configure.CONNECTION_TIMEOUT_IN_SECOND, TimeUnit.SECONDS)


        return builder.build()
    }
}