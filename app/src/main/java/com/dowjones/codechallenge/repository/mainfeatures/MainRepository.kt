package com.dowjones.codechallenge.repository.mainfeatures

import androidx.lifecycle.MutableLiveData
import com.dowjones.codechallenge.dto.CodeChallengeDto
import com.dowjones.codechallenge.helper.groupById
import com.dowjones.codechallenge.model.GroupByIdModel
import com.dowjones.codechallenge.repository.base.BaseRepository
import com.dowjones.codechallenge.service.TypiCodeService
import io.reactivex.Observable

class MainRepository : BaseRepository() {
    private val typiService = TypiCodeService()
    var typiServiceCode = MutableLiveData<List<GroupByIdModel>>()

    fun getTypiCodeData(
    ): Observable<List<CodeChallengeDto>>? {
        return typiService.getTypiCodeData()
            .doOnNext { data ->
                typiServiceCode.postValue(data.groupById()) }

    }

    companion object {
        private var sInstance: MainRepository? = null

        @JvmStatic
        @get:Synchronized
        val instance: MainRepository?
            get() {
                if (sInstance == null) {
                    sInstance = MainRepository()
                }
                return sInstance
            }

        @JvmStatic
        fun close() {
            sInstance = null
        }
    }


}