package com.dowjones.codechallenge.util

import com.bumptech.glide.load.model.GlideUrl
import com.bumptech.glide.load.model.LazyHeaders

open class HttpHeaderUtil {
    companion object{
        fun addHeaderUrl(url: String) = GlideUrl(
            url, LazyHeaders.Builder()
                .addHeader("User-Agent" , "Mozilla/5.0 (Macintosh; U; Intel Mac OS X; ja-JP-mac; rv:1.8.1.6) Gecko/20070725 Firefox/2.0.0.6")
                .build()
        )
    }
}