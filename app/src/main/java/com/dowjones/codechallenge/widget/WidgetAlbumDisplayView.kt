package com.dowjones.codechallenge.widget

import android.content.Context
import android.graphics.drawable.Drawable
import android.util.AttributeSet
import android.view.LayoutInflater
import com.bumptech.glide.Glide
import com.bumptech.glide.load.model.GlideUrl
import com.bumptech.glide.request.target.CustomTarget
import com.bumptech.glide.request.transition.Transition
import com.dowjones.codechallenge.databinding.WidgetAlbumDisplayViewBinding
import com.dowjones.codechallenge.dto.CodeChallengeDto
import com.google.android.material.card.MaterialCardView
import okhttp3.internal.notifyAll

class WidgetAlbumDisplayView @JvmOverloads constructor(
    context: Context, attrs: AttributeSet? = null, defStyleAttr: Int = 0
) : MaterialCardView(context, attrs, defStyleAttr) {

    private var binding: WidgetAlbumDisplayViewBinding =
        WidgetAlbumDisplayViewBinding.inflate(LayoutInflater.from(context), this)

    fun setImage(imgURI: GlideUrl, context: Context) {
        Glide.with(context)
            .load(imgURI)
            .into(object : CustomTarget<Drawable?>() {
                override fun onResourceReady(
                    resource: Drawable,
                    transition: Transition<in Drawable?>?
                ) {
                    binding.imgCard.setImageDrawable(resource)
                }
                override fun onLoadCleared(placeholder: Drawable?) {
                    binding.imgCard.setImageDrawable(placeholder)
                }
            })
    }

    fun setTitle(title: String?) {
        binding.albumTitle.visibility = if (title == null) GONE else visibility
        binding.albumTitle.text = title
    }

    fun setCount(count: String?) {
        binding.albumCount.visibility = if (count == null) GONE else visibility
        binding.albumCount.text = count
    }

    fun setAction(action: () -> Unit) {
        binding.imgCard.setOnClickListener {
            action.invoke()
        }
    }
    fun getTitle():String {
        return binding.albumTitle.text.toString()
    }

    fun getCount():String {
        return binding.albumCount.text.toString()
    }

}