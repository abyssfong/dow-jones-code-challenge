package com.dowjones.codechallenge.widget

import android.content.Context
import android.graphics.drawable.Drawable
import android.util.AttributeSet
import android.view.LayoutInflater
import com.bumptech.glide.Glide
import com.bumptech.glide.load.model.GlideUrl
import com.bumptech.glide.request.target.CustomTarget
import com.bumptech.glide.request.transition.Transition
import com.dowjones.codechallenge.databinding.WidgetAlbumDisplayViewBinding
import com.dowjones.codechallenge.databinding.WidgetAlbumPhotoInsideViewBinding
import com.dowjones.codechallenge.dto.CodeChallengeDto
import com.google.android.material.card.MaterialCardView

class WidgetAlbumPhotoInsideView @JvmOverloads constructor(
    context: Context, attrs: AttributeSet? = null, defStyleAttr: Int = 0
) : MaterialCardView(context, attrs, defStyleAttr) {

    private var binding: WidgetAlbumPhotoInsideViewBinding =
        WidgetAlbumPhotoInsideViewBinding.inflate(LayoutInflater.from(context), this)

    fun setImage(imgURI: GlideUrl, context: Context) {
        Glide.with(context)
            .load(imgURI)
            .into(object : CustomTarget<Drawable?>() {
                override fun onResourceReady(
                    resource: Drawable,
                    transition: Transition<in Drawable?>?
                ) {
                    binding.imgCard.setImageDrawable(resource)
                }

                override fun onLoadCleared(placeholder: Drawable?) {
                    binding.imgCard.setImageDrawable(placeholder)
                }
            })
    }

    fun setTitle(title: String?) {
        binding.albumInsideTitle.visibility = if (title == null) GONE else visibility
        binding.albumInsideTitle.text = title
    }

    fun getTitle(): String {
        return binding.albumInsideTitle.text.toString()
    }

    fun setAction(action: () -> Unit) {
        binding.imgCard.setOnClickListener {
            action.invoke()
        }
    }
}