package com.dowjones.codechallenge.widget

import android.content.Context
import android.util.AttributeSet
import android.view.LayoutInflater
import android.view.View
import android.widget.FrameLayout
import android.widget.RelativeLayout
import androidx.annotation.DrawableRes
import androidx.annotation.StringRes
import com.dowjones.codechallenge.databinding.WidgetTopBarBinding
import okhttp3.internal.wait


class WidgetToolBar @JvmOverloads constructor(
    context: Context,
    attrs: AttributeSet? = null,
    defStyleAttr: Int = 0
) : FrameLayout(context, attrs, defStyleAttr) {
    private var binding: WidgetTopBarBinding =
        WidgetTopBarBinding.inflate(LayoutInflater.from(context), this, true)

    fun setTitle(title: CharSequence) {
        binding.textViewToolbarTitle.text = title
    }

    fun getTitle() :String{
        return binding.textViewToolbarTitle.text.toString()
    }
}
