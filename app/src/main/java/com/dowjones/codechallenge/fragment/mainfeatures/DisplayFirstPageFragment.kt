package com.dowjones.codechallenge.fragment.mainfeatures


import androidx.fragment.app.viewModels
import androidx.navigation.Navigation
import androidx.recyclerview.widget.GridLayoutManager
import com.dowjones.codechallenge.R
import com.dowjones.codechallenge.adapter.MainPagePhotoAdapter
import com.dowjones.codechallenge.databinding.DisplayFirstPageFragmentBinding
import com.dowjones.codechallenge.fragment.base.BaseFragment
import com.dowjones.codechallenge.helper.viewBinding
import com.dowjones.codechallenge.model.GroupByIdModel
import com.dowjones.codechallenge.viewmodel.mainfeatures.DisplayFirstPageViewModel

class DisplayFirstPageFragment : BaseFragment(R.layout.display_first_page_fragment) {

    private val binding by viewBinding(DisplayFirstPageFragmentBinding::bind)
    private val displayFirstPageVM by viewModels<DisplayFirstPageViewModel>()
    private var adapter: MainPagePhotoAdapter? = null
    private var dataList: List<GroupByIdModel>? = null

    override fun initLayout() {
        super.initLayout()
        binding.topbar.setTitle("MainPage")
        adapter = MainPagePhotoAdapter()
        binding.MainPagePhotoRecycleView.layoutManager = GridLayoutManager(context,2)
        binding.MainPagePhotoRecycleView.adapter = adapter
        adapter!!.onItemClick = { position: Int ->
            run {
                Navigation.findNavController(binding.root)
                    .navigate(DisplayFirstPageFragmentDirections
                        .actionDisplayFirstPageFragmentToDisplayInsideFragment(position))
            }
        }
        binding.swipeRefreshLayout.setOnRefreshListener {
             displayFirstPageVM.getTypiCodeData()
        }
        observeTypiServiceCode()
    }

    private fun observeTypiServiceCode() {
        displayFirstPageVM.getAlbumList()?.observe(viewLifecycleOwner) { list ->
            dataList = list
            adapter?.updateData(list)
            binding.swipeRefreshLayout.isRefreshing = false
        }
    }

}