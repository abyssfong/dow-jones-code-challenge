package com.dowjones.codechallenge.fragment.mainfeatures

import android.graphics.drawable.Drawable
import android.opengl.Visibility
import android.view.View
import com.bumptech.glide.Glide
import com.bumptech.glide.request.target.CustomTarget
import com.bumptech.glide.request.transition.Transition
import com.dowjones.codechallenge.R
import com.dowjones.codechallenge.databinding.DisplayFullImageFragmentBinding
import com.dowjones.codechallenge.fragment.base.BaseFragment
import com.dowjones.codechallenge.helper.viewBinding
import com.dowjones.codechallenge.util.HttpHeaderUtil.Companion.addHeaderUrl

class DisplayFullImageFragment: BaseFragment(R.layout.display_full_image_fragment) {

    private val binding by viewBinding(DisplayFullImageFragmentBinding::bind)

    override fun initLayout() {
        super.initLayout()
        val imgUrl = arguments?.let { DisplayFullImageFragmentArgs.fromBundle(it).imgUrl }
        Glide.with(requireContext())
            .load(imgUrl?.let { addHeaderUrl(it) })
            .into(object : CustomTarget<Drawable?>() {
                override fun onResourceReady(
                    resource: Drawable,
                    transition: Transition<in Drawable?>?
                ) {
                    binding.tvLoading.visibility = View.GONE
                    binding.imgBigImage.setImageDrawable(resource)
                }
                override fun onLoadCleared(placeholder: Drawable?) {
//                    binding.imgBigImage.setImageDrawable(placeholder)
                }
                override fun onLoadStarted(placeholder: Drawable?) {
                    binding.imgBigImage.setImageDrawable(placeholder)
                    binding.tvLoading.visibility = View.VISIBLE
                }
            })
    }
}