package com.dowjones.codechallenge.fragment.base

import android.os.Bundle
import android.view.View
import androidx.annotation.LayoutRes
import androidx.fragment.app.Fragment
import com.dowjones.codechallenge.activity.mainfeatures.MainActivity

abstract class BaseFragment(@LayoutRes contentLayout: Int) : Fragment(contentLayout) {
    open fun initLayout() {}

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        initLayout()
    }
}