package com.dowjones.codechallenge.fragment.mainfeatures

import androidx.fragment.app.viewModels
import androidx.navigation.Navigation
import androidx.recyclerview.widget.GridLayoutManager
import com.dowjones.codechallenge.fragment.base.BaseFragment
import com.dowjones.codechallenge.R
import com.dowjones.codechallenge.adapter.AlbumPhotoAdapter
import com.dowjones.codechallenge.databinding.DisplayInsideFragmentBinding
import com.dowjones.codechallenge.helper.viewBinding
import com.dowjones.codechallenge.viewmodel.mainfeatures.DisplayInsideViewModel

class DisplayInsideFragment : BaseFragment(R.layout.display_inside_fragment){

    private val binding by viewBinding(DisplayInsideFragmentBinding::bind)
    private val displayInsideVM by viewModels<DisplayInsideViewModel>()
    private var adapter: AlbumPhotoAdapter? = null

    override fun initLayout() {
        super.initLayout()
        val position = getArguments()?.let { DisplayInsideFragmentArgs.fromBundle(it).groupPosition }
        adapter = AlbumPhotoAdapter()
        binding.insideRecycleView.layoutManager = GridLayoutManager(context,2)
        binding.insideRecycleView.adapter = adapter
        adapter!!.onItemClick = { uri: String ->
            run {
                Navigation.findNavController(binding.root)
                    .navigate(DisplayInsideFragmentDirections.actionDisplayInsideFragmentToDisplayFullImageFragment().setImgUrl(uri))
            }
        }
        if (position != null){
            observePhotoData(position)
        }

    }
    private fun observePhotoData(position: Int){
        displayInsideVM.getAlbumList()?.observe(viewLifecycleOwner) { list ->
            adapter?.updateData(list[position].imgData)
        }
    }

}