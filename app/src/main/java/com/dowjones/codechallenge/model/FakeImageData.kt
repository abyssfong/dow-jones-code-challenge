package com.dowjones.codechallenge.model

import com.dowjones.codechallenge.dto.CodeChallengeDto

object FakeImageData {

    val photos = arrayOf(
        CodeChallengeDto(
            1,
            1,
            "accusamus beatae ad facilis cum similique qui sunt",
            "https://via.placeholder.com/600/92c952",
            "https://via.placeholder.com/150/92c952"
        ),
        CodeChallengeDto(
            2,
            1,
            "reprehenderit est deserunt velit ipsam",
            "https://via.placeholder.com/600/771796\"",
            "https://via.placeholder.com/150/771796"
        )
    )
}