package com.dowjones.codechallenge.model

import com.dowjones.codechallenge.dto.CodeChallengeDto

data class GroupByIdModel(val albumId: Long?, val imgData: List<CodeChallengeDto>)
