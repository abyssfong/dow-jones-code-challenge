package com.dowjones.codechallenge.activity.mainfeatures

import android.os.Bundle
import com.dowjones.codechallenge.activity.base.BaseActivity
import com.dowjones.codechallenge.databinding.MainActivityBinding
import com.dowjones.codechallenge.helper.viewBinding

class MainActivity : BaseActivity() {

    private val binding by viewBinding(MainActivityBinding::inflate)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(binding.root)
    }
}