package com.dowjones.codechallenge.activity.mainfeatures

import android.content.Intent
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.util.Log
import android.widget.Toast
import androidx.activity.viewModels
import com.dowjones.codechallenge.R
import com.dowjones.codechallenge.activity.base.BaseActivity
import com.dowjones.codechallenge.databinding.SplashActivityBinding
import com.dowjones.codechallenge.helper.viewBinding
import com.dowjones.codechallenge.service.Constants
import com.dowjones.codechallenge.viewmodel.mainfeatures.SplashActivityViewModel


class SplashActivity : BaseActivity() {
    private val SPLASH_DISPLAY_LENGTH: Long = 1000
    private val binding by viewBinding(SplashActivityBinding::inflate)
    private val splashVM by viewModels<SplashActivityViewModel>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(binding.root)
        splashVM.getTypiCodeData()
        observeAppEven()
    }

    private fun observeAppEven() {
        splashVM.event.observe(this) { event ->
            when (event.name) {
                Constants.EVENT_SUCCESS -> landToNextPage()
                else ->{
                    Toast.makeText(this,getString(R.string.api_error),Toast.LENGTH_LONG).show()
                    landToNextPage()
                    Log.d("error", "api Error")
                }
            }
        }
    }

    private fun landToNextPage() {
        Handler(Looper.getMainLooper()).postDelayed({
            /* Create an Intent that will start the Menu-Activity. */
            val mainIntent = Intent(this@SplashActivity, MainActivity::class.java)
            this@SplashActivity.startActivity(mainIntent)
            this@SplashActivity.finish()
        }, SPLASH_DISPLAY_LENGTH)
    }
}