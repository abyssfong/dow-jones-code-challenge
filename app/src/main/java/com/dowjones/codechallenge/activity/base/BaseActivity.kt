package com.dowjones.codechallenge.activity.base

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.dowjones.codechallenge.R

open class BaseActivity : AppCompatActivity() {

}