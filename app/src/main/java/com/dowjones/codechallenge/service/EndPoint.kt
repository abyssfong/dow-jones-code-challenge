package com.dowjones.codechallenge.service

import com.dowjones.codechallenge.dto.CodeChallengeDto
import io.reactivex.Observable
import retrofit2.http.GET

interface EndPoint {
    @GET("photos")
    fun getTypiCodeData(
    ): Observable<List<CodeChallengeDto>>
}