package com.dowjones.codechallenge.service

import android.content.Context
import com.dowjones.codechallenge.dto.CodeChallengeDto
import io.reactivex.Observable

class TypiCodeService : BaseService<EndPoint>() {
    fun getTypiCodeData(): Observable<List<CodeChallengeDto>> {
        return serviceInstance.getTypiCodeData()
    }

    override val endpointClass: Class<EndPoint>
        get() = EndPoint::class.java
}