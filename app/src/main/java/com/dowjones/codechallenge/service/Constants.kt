package com.dowjones.codechallenge.service

open class Constants {
    companion object {
        const val EVENT_SUCCESS = "success"
        const val EVENT_ERROR = "error"
    }
}