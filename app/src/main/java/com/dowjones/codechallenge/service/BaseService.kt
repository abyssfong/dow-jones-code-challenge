package com.dowjones.codechallenge.service

import com.dowjones.codechallenge.connection.RetrofitClient

abstract class BaseService<EndpointClass> {
    protected val serviceInstance: EndpointClass
    protected abstract val endpointClass: Class<EndpointClass>

    init {
        serviceInstance = RetrofitClient().getDefaultInstance()?.create(endpointClass)!!
    }
}