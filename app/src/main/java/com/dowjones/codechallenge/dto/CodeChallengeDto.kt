package com.dowjones.codechallenge.dto

data class CodeChallengeDto (
    val albumId: Long,
    val id: Long,
    val title: String,
    val url: String,
    val thumbnailUrl: String
)