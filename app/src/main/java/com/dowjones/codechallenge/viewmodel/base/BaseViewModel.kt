package com.dowjones.codechallenge.viewmodel.base

import androidx.lifecycle.LiveData
import androidx.lifecycle.LiveDataReactiveStreams
import androidx.lifecycle.ViewModel
import com.dowjones.codechallenge.model.AppEvent
import io.reactivex.BackpressureStrategy
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.subjects.PublishSubject

open class BaseViewModel : ViewModel(){
    @JvmField
    protected var mCompositeDisposable = CompositeDisposable()
    private val mEvent = PublishSubject.create<AppEvent>()

    fun disposeCompositeDisposable() {
        mCompositeDisposable.dispose()
        mCompositeDisposable = CompositeDisposable()
    }

    fun clearCompositeDisposable() {
        mCompositeDisposable.clear()
    }

    override fun onCleared() {
        super.onCleared()
        mCompositeDisposable.dispose()
    }

    protected fun sendEvent(event: AppEvent) {
        mEvent.onNext(event)
    }

    val event: LiveData<AppEvent>
        get() = LiveDataReactiveStreams.fromPublisher(mEvent.toFlowable(BackpressureStrategy.LATEST))

}