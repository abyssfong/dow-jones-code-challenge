package com.dowjones.codechallenge.viewmodel.mainfeatures

import android.content.Context
import android.util.Log
import androidx.lifecycle.MutableLiveData
import com.dowjones.codechallenge.dto.CodeChallengeDto
import com.dowjones.codechallenge.model.AppEvent
import com.dowjones.codechallenge.model.GroupByIdModel
import com.dowjones.codechallenge.repository.mainfeatures.MainRepository
import com.dowjones.codechallenge.service.Constants
import com.dowjones.codechallenge.viewmodel.base.BaseViewModel
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.observers.DisposableObserver
import io.reactivex.schedulers.Schedulers


class DisplayFirstPageViewModel : BaseViewModel() {

    fun getTypiCodeData() {
        MainRepository.instance?.getTypiCodeData()
            ?.subscribeOn(Schedulers.io())
            ?.observeOn(AndroidSchedulers.mainThread())?.let {
                mCompositeDisposable.add(
                    it.subscribeWith(object :
                        DisposableObserver<List<CodeChallengeDto>?>() {
                        override fun onNext(productPage: List<CodeChallengeDto>) {
                            sendEvent(AppEvent(Constants.EVENT_SUCCESS))
                        }

                        override fun onError(throwable: Throwable) {
                            sendEvent(AppEvent(Constants.EVENT_ERROR))
                        }

                        override fun onComplete() {
                            Log.d("here", "onComplete")
                        }
                    })
                )
            }
    }

    fun getAlbumList(): MutableLiveData<List<GroupByIdModel>>? {
       return MainRepository.instance?.typiServiceCode
    }

}