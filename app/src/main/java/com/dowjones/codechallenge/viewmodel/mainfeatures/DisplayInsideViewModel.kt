package com.dowjones.codechallenge.viewmodel.mainfeatures

import android.content.Context
import android.util.Log
import androidx.lifecycle.MutableLiveData
import com.dowjones.codechallenge.dto.CodeChallengeDto
import com.dowjones.codechallenge.model.AppEvent
import com.dowjones.codechallenge.model.GroupByIdModel
import com.dowjones.codechallenge.repository.mainfeatures.MainRepository
import com.dowjones.codechallenge.service.Constants
import com.dowjones.codechallenge.viewmodel.base.BaseViewModel
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.observers.DisposableObserver
import io.reactivex.schedulers.Schedulers


class DisplayInsideViewModel : BaseViewModel() {

    fun getAlbumList(): MutableLiveData<List<GroupByIdModel>>? {
       return MainRepository.instance?.typiServiceCode
    }

}