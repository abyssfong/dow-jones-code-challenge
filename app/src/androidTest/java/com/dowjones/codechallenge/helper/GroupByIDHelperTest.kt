package com.dowjones.codechallenge.helper

import android.content.Intent
import android.content.res.Resources
import android.util.Log
import androidx.lifecycle.MutableLiveData
import androidx.test.rule.ActivityTestRule
import com.dowjones.codechallenge.R
import com.dowjones.codechallenge.activity.mainfeatures.MainActivity
import com.dowjones.codechallenge.dto.CodeChallengeDto
import com.dowjones.codechallenge.model.GroupByIdModel
import com.google.gson.Gson
import org.junit.Assert
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import java.io.ByteArrayOutputStream
import java.io.IOException

class GroupByIDHelperTest {

    private var launchedActivity: MainActivity? = null

    @get:Rule
    val activityRule =
        object : ActivityTestRule<MainActivity>(MainActivity::class.java, false, false) {}

    @Before
    fun setup() {
        val intent = Intent(Intent.ACTION_PICK)
        intent.putExtra("parameter", "Value")
        launchedActivity = activityRule.launchActivity(intent)
    }

    @Test
    fun checkGroupById(){
        val jsonString: String =
            launchedActivity?.resources?.let { readStringForRawFile(it, R.raw.mock) }.toString()
        val gson = Gson()
        val CodeChallengeDtoList: Array<CodeChallengeDto> = gson.fromJson(
            jsonString,
            Array<CodeChallengeDto>::class.java
        )
        Assert.assertNotNull( CodeChallengeDtoList.toList().groupById())
    }

    fun readStringForRawFile(resources: Resources, id: Int): String {
            val inputStream = resources.openRawResource(id)
            val outputStream = ByteArrayOutputStream()
            val buf = ByteArray(1024)
            var len: Int
            try {
                while (inputStream.read(buf).also { len = it } != -1) {
                    outputStream.write(buf, 0, len)
                }
                outputStream.close()
                inputStream.close()
            } catch (e: IOException) {
                Log.d("Error", e.toString())
            } finally {
                try {
                    outputStream.close()
                    inputStream.close()
                } catch (e: Exception) {
                    Log.d("Error", e.toString())
                }
            }

            return outputStream.toString()
        }
}