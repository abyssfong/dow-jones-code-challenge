package com.dowjones.codechallenge.testrule

import android.app.Activity
import android.app.Instrumentation
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.annotation.LayoutRes
import androidx.test.platform.app.InstrumentationRegistry
import androidx.test.rule.ActivityTestRule
import com.dowjones.codechallenge.R
import org.hamcrest.Description
import org.hamcrest.Matcher
import org.hamcrest.TypeSafeMatcher

class ViewTestRule<T : View?> protected constructor(
    var instrumentation: Instrumentation,
    private val viewCreator: ViewCreator<T>
) : ActivityTestRule<TestingActivity>(
    TestingActivity::class.java
) {
    var view: T? = null
        private set

    constructor(@LayoutRes layoutId: Int) : this(InflateFromXmlViewCreator<T>(layoutId)) {}
    constructor(viewCreator: ViewCreator<T>) : this(
        InstrumentationRegistry.getInstrumentation(),
        viewCreator
    ) {
    }

    fun runOnMainSynchronously(runner: Runner<T>) {
        instrumentation.runOnMainSync { view?.let { runner.run(it) } }
    }

    fun addView(view: View?) {
        instrumentation.runOnMainSync {
            val viewGroup = this@ViewTestRule.view as ViewGroup
            viewGroup.addView(view)
        }
    }

    fun updateView(view: T) {
        instrumentation.runOnMainSync {
            val viewGroup = this@ViewTestRule.view as ViewGroup
            viewGroup.removeAllViews()
            viewGroup.addView(view)
            this.view = view
        }
    }

    fun updateActivityToNewLayout(@LayoutRes layoutRes: Int) {
        val inflatedView = LayoutInflater.from(activity).inflate(layoutRes, null)
        updateView(inflatedView as T)
    }

    fun updateActivityToNewView(view: View) {
        updateView(view as T)
    }

    fun waitForIdleSync() {
        instrumentation.waitForIdleSync()
    }

    private fun createView(activity: Activity?) {
        instrumentation.runOnMainSync {
            view = viewCreator.createView(
                activity,
                activity!!.findViewById<View>(android.R.id.content) as ViewGroup
            )
            view!!.setTag(R.id.espresso_support_view_test_rule, true)
        }
    }

    override fun afterActivityLaunched() {
        super.afterActivityLaunched()
        val activity = activity
        createView(activity)
        runOnMainSynchronously(object : Runner<T> {
            override fun run(view: T) {
                activity!!.setContentView(
                    view,
                    view!!.layoutParams
                )
            }
        })
    }

    interface Runner<T> {
        fun run(view: T)
    }

    companion object {
        fun underTest(): Matcher<View> {
            return object : TypeSafeMatcher<View>() {
                override fun matchesSafely(item: View): Boolean {
                    val tag = item.getTag(R.id.espresso_support_view_test_rule)
                    return tag != null && tag as Boolean
                }

                override fun describeTo(description: Description) {
                    description.appendText("is View under test, managed by this " + ViewTestRule::class.java.simpleName)
                }
            }
        }
    }
}