package com.dowjones.codechallenge.testrule

import android.content.Context
import android.view.View
import android.view.ViewGroup

interface ViewCreator<T : View?> {
    /**
     * Create the view, inflating or programmatically. Do not attach the created view to the parentView.
     *
     * @param context    Activity Context
     * @param parentView Activity root content View
     */
    fun createView(context: Context?, parentView: ViewGroup?): T
}