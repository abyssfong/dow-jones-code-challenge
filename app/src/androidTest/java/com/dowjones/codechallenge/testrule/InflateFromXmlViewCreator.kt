package com.dowjones.codechallenge.testrule

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.annotation.LayoutRes

internal class InflateFromXmlViewCreator<T : View?>(@field:LayoutRes @param:LayoutRes private val id: Int) :
    ViewCreator<T> {
    override fun createView(context: Context?, parentView: ViewGroup?): T {
        val layoutInflater = LayoutInflater.from(context)
        return layoutInflater.inflate(id, parentView, false) as T
    }
}