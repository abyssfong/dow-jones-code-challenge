package com.dowjones.codechallenge.widget

import androidx.constraintlayout.widget.ConstraintLayout
import androidx.test.ext.junit.runners.AndroidJUnit4
import com.dowjones.codechallenge.R
import com.dowjones.codechallenge.testrule.ViewTestRule
import org.junit.Assert
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith

@RunWith(AndroidJUnit4::class)
class WidgetToolBarTest {
    @get:Rule
    var viewTestRule: ViewTestRule<ConstraintLayout> = ViewTestRule(R.layout.display_first_page_fragment)
    var widgetToolBar: WidgetToolBar? = null


    @Before
    fun setUp() {
        widgetToolBar = viewTestRule.view?.findViewById(R.id.topbar)
    }

    @Test
    fun checkTestSet(){
        val testString = "test"
        widgetToolBar?.setTitle("test")
        Assert.assertEquals(widgetToolBar?.getTitle(),testString)
    }

    @Test
    fun checkInit(){
        Assert.assertNotNull(WidgetToolBar(viewTestRule.activity))
    }


}