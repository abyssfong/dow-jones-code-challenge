package com.dowjones.codechallenge.widget

import android.widget.LinearLayout
import androidx.test.ext.junit.runners.AndroidJUnit4
import com.dowjones.codechallenge.R
import com.dowjones.codechallenge.testrule.ViewTestRule
import com.dowjones.codechallenge.util.HttpHeaderUtil
import org.junit.Assert
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith

@RunWith(AndroidJUnit4::class)
class WidgetAlbumPhotoInsideViewTest {
    @get:Rule
    var viewTestRule: ViewTestRule<LinearLayout> = ViewTestRule(R.layout.cell_inside_adapter_item)
    var widgetAlbumPhotoInsideView: WidgetAlbumPhotoInsideView? = null

    @Before
    fun setUp() {
        widgetAlbumPhotoInsideView = viewTestRule.view?.findViewById(R.id.widgetAlbumPhotoInsideView)
    }

    @Test
    fun checkTestSet(){
        val testString = "test"
        widgetAlbumPhotoInsideView?.setTitle(testString)
        Assert.assertEquals(widgetAlbumPhotoInsideView?.getTitle(),testString)
    }

    @Test
    fun checkInit(){
        Assert.assertNotNull(WidgetAlbumPhotoInsideView(viewTestRule.activity))
    }

    @Test
    fun checkImageSet(){
        widgetAlbumPhotoInsideView?.context?.let {
            widgetAlbumPhotoInsideView?.setImage(
                HttpHeaderUtil.addHeaderUrl("https://via.placeholder.com/150/771796"),
                it
            )
        }
    }

}