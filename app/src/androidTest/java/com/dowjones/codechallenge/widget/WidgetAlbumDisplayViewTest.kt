package com.dowjones.codechallenge.widget

import android.widget.LinearLayout
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.test.ext.junit.runners.AndroidJUnit4
import com.dowjones.codechallenge.R
import com.dowjones.codechallenge.databinding.CellMainAdapterItemBinding
import com.dowjones.codechallenge.testrule.ViewTestRule
import com.dowjones.codechallenge.util.HttpHeaderUtil
import org.junit.Assert
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith


@RunWith(AndroidJUnit4::class)
class WidgetAlbumDisplayViewTest {
    @get:Rule
    var viewTestRule: ViewTestRule<LinearLayout> = ViewTestRule(R.layout.cell_main_adapter_item)
    var widgetAlbumDisplayView: WidgetAlbumDisplayView? = null

    @Before
    fun setUp() {
        widgetAlbumDisplayView = viewTestRule.view?.findViewById(R.id.widgetAlbumDisplayView)
    }

    @Test
    fun checkTestSet(){
        val testString = "test"
        widgetAlbumDisplayView?.setTitle(testString)
        Assert.assertEquals(widgetAlbumDisplayView?.getTitle(),testString)
    }

    @Test
    fun checkCountSet(){
        val testString = "test"
        widgetAlbumDisplayView?.setCount(testString)
        Assert.assertEquals(widgetAlbumDisplayView?.getCount(),testString)
    }

    @Test
    fun checkInit(){
        Assert.assertNotNull(WidgetAlbumDisplayView(viewTestRule.activity))
    }

    @Test
    fun checkImageSet(){
        widgetAlbumDisplayView?.context?.let {
            widgetAlbumDisplayView?.setImage(HttpHeaderUtil.addHeaderUrl("https://via.placeholder.com/150/771796"),
                it
            )
        }
    }

}