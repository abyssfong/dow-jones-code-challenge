package com.dowjones.codechallenge.fragment


import androidx.test.espresso.Espresso
import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.action.ViewActions
import androidx.test.espresso.assertion.ViewAssertions
import androidx.test.espresso.contrib.RecyclerViewActions
import androidx.test.espresso.matcher.ViewMatchers.*
import androidx.test.ext.junit.rules.ActivityScenarioRule

import androidx.test.ext.junit.runners.AndroidJUnit4
import com.dowjones.codechallenge.R
import com.dowjones.codechallenge.activity.mainfeatures.MainActivity
import com.dowjones.codechallenge.adapter.AlbumPhotoAdapter
import com.dowjones.codechallenge.helper.groupById
import com.dowjones.codechallenge.model.FakeImageData
import com.dowjones.codechallenge.repository.mainfeatures.MainRepository
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith


@RunWith(AndroidJUnit4::class)
class DisplayFirstPageFragmentTest {

    @get:Rule
    val activityRule = ActivityScenarioRule(MainActivity::class.java)
    val LIST_ITEM_IN_TEST = 0
    val PHOTO_IN_TEST = FakeImageData.photos[LIST_ITEM_IN_TEST]

    @Before
    fun setUp() {
        //Set UP date
        MainRepository.instance?.typiServiceCode?.postValue(
            FakeImageData.photos.toList().groupById()
        )
    }

    @Test
    fun test_isListFragmentVisible_onAppLaunch() {
        onView(withId(R.id._main_page_photo_recycle_view)).check(
            ViewAssertions.matches(
                isDisplayed()
            )
        )
    }

    @Test
    fun test_selectListItem_isNavigation() {

        // Click list item #LIST_ITEM_IN_TEST
        onView(withId(R.id._main_page_photo_recycle_view))
            .perform(
                RecyclerViewActions.actionOnItemAtPosition<AlbumPhotoAdapter.AlbumPhotoAdapterHorder>(
                    LIST_ITEM_IN_TEST,
                    ViewActions.click()
                )
            )
        // Confirm nav to DetailFragment and display title
        onView(withId(R.id.album_inside_title)).check(ViewAssertions.matches(withText(PHOTO_IN_TEST.title)))
    }

    @Test
    fun test_backNavigation_toMainListFragment() {
        // Click list item #LIST_ITEM_IN_TEST
        onView(withId(R.id._main_page_photo_recycle_view))
            .perform(
                RecyclerViewActions.actionOnItemAtPosition<AlbumPhotoAdapter.AlbumPhotoAdapterHorder>(
                    LIST_ITEM_IN_TEST,
                    ViewActions.click()
                )
            )
        // Confirm nav to DetailFragment and display title
        onView(withId(R.id.album_inside_title)).check(ViewAssertions.matches(withText(PHOTO_IN_TEST.title)))
        Espresso.pressBack()

        // Confirm MovieListFragment in view
        onView(withId(R.id._main_page_photo_recycle_view)).check(
            ViewAssertions.matches(
                isDisplayed()
            )
        )
    }
}