package com.dowjones.codechallenge.util

import androidx.test.ext.junit.runners.AndroidJUnit4
import com.bumptech.glide.load.model.GlideUrl
import com.bumptech.glide.load.model.LazyHeaders
import org.junit.Assert
import org.junit.Test
import org.junit.runner.RunWith


@RunWith(AndroidJUnit4::class)
class HeaderUtilTest {
    @Test
    fun HttpHeaderUtilTest(){
        val uri:String = "test.com"
        val result =GlideUrl(
            uri, LazyHeaders.Builder()
                .addHeader("User-Agent" , "Mozilla/5.0 (Macintosh; U; Intel Mac OS X; ja-JP-mac; rv:1.8.1.6) Gecko/20070725 Firefox/2.0.0.6")
                .build()
        )
        Assert.assertEquals(result,HttpHeaderUtil.addHeaderUrl(uri))
    }

    @Test
    fun InitHttpHeaderUtilTest(){
        Assert.assertNotNull(HttpHeaderUtil())
    }
}