package com.dowjones.codechallenge.activity

import android.content.Intent
import androidx.test.ext.junit.runners.AndroidJUnit4
import androidx.test.platform.app.InstrumentationRegistry
import androidx.test.rule.ActivityTestRule
import com.dowjones.codechallenge.BuildConfig
import com.dowjones.codechallenge.activity.mainfeatures.MainActivity
import junit.framework.Assert
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith

@RunWith(AndroidJUnit4::class)
class MainActivityTest {

    private var launchedActivity: MainActivity? = null

    @get:Rule
    val activityRule =
        object : ActivityTestRule<MainActivity>(MainActivity::class.java, false, false) {}

    @Before
    fun setup() {
        val intent = Intent(Intent.ACTION_PICK)
        intent.putExtra("parameter", "Value")
        launchedActivity = activityRule.launchActivity(intent)
    }

    @Test
    fun useAppContext() {
        // Context of the app under test.
        val appContext = InstrumentationRegistry.getInstrumentation().context
        Assert.assertEquals("com.dowjones.codechallenge.test", appContext.packageName)
    }



}